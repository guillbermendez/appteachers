import { makeStyles } from '@material-ui/core/styles';
import React, {  useState } from "react";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import CardTeacher from './components/CardTeacher';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import { CardTravelSharp, CastForEducationSharp } from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  paper: { 
  flexGrow: 1,
  padding: theme.spacing(3),
  textAlign: 'center',
  color: theme.palette.text.secondary,
},
toolBar:{
  textAlign: 'right',
}
}));


function App() {
const classes = useStyles();

const [teachers, setTeachers] = useState(
  [
    {nombre: 'Mtro. Iván Antonio', materia: 'Seguridad en el Desarrollo de Software', horario:  'martes 16-18, jueves 14 -16'},
    {nombre: 'Mtro. David García', materia: 'Matemáticas para Ingeniería', horario: 'miércoles 15-18, viernes 16-18'},
    {nombre: 'Mtro. Alfonso Felipe', materia: 'Desarrollo Web Profesional', horario: 'miércoles 13-15, viernes 13-15'},
    {nombre: 'Mtra. Alejandra Morán', materia: 'Planeación y Organización del trabajo', horario: 'martes 14-16, jueves 13-14'},
]
);



const handleAgregarC = () =>{
  console.log("Se agrego un nuevo registro")
  //Actualizar state
  setTeachers([
   ...teachers,
   {nombre: '', materia: '', horario: ''}
  ]);
};

const handleEliminar = (index) => {
//eliminar elementos
   teachers.splice(index, 1);
   setTeachers([...teachers]);
};

const renderCard = () => {
  //Renderizado del array
  return teachers.map((teacher, index) => (
    <Grid key={index} item xs={12} sm={3}>
    <CardTeacher datos={teacher} index={index} onDelete={handleEliminar} />
  </Grid>
  ));
};

  return (
  <Paper className={classes.paper}>
    <Grid container spacing={3}>
      <Grid item xs={12} className={classes.toolBar}>
        <Button
        variant="contained"
        color="primary"
        startIcon={<Icon>add</Icon>}
        onClick={handleAgregarC}
        >
          Agregar
        </Button>
      </Grid>
     {renderCard()}
    </Grid>
  </Paper>
  );
}

export default App;
